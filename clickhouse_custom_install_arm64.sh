#!/bin/sh

#install run from https://github.com/yandex/ClickHouse/issues/3843 

sudo apt-get install -y debhelper pbuilder fakeroot clang

git clone https://github.com/yandex/ClickHouse.git
cd ClickHouse

git submodule update --init --recursive

env DIST=bionic DISABLE_PARALLEL=1 CMAKE_FLAGS="-DHAVE_SSE41=0 -DHAVE_SSE42=0 -DHAVE_SSSE3=0 -DHAVE_AVX2=0 -DHAVE_AVX=0 -DHAVE_POPCNT=0 -DHAVE_SSE2_INTRIN=0 -DSSE2~AG=' ' -DHAVE_PCLMULQDQ_INTRIN=0 -DPCLMULFLAG=' '" EXTRAPACKAGES="clang-6.0 libstdc++-8-dev" DEB_CC=clang-6.0 DEB_CXX=clang++-6.0 CMAKE_FLAGS=" -DNO_WERROR=1 " ./release