#!/bin/sh

sudo apt-get remove docker docker-engine docker.io containerd runc

sudo apt-get update

#Install packages to allow apt to use a repository over HTTPS:
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
	
#Add Docker’s official GPG key:	
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

#test key
sudo apt-key fingerprint 0EBFCD88


sudo add-apt-repository \
   "deb [arch=arm64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
   
   
 sudo apt-get update
 
 
 sudo apt-get install docker-ce docker-ce-cli containerd.io
 
 
 sudo docker run hello-world