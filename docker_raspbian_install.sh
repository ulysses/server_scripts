#!/bin/sh

#install docker on raspbian
curl -fsSL https://get.docker.com -o get-docker.sh

sh get-docker.sh

sudo usermod -aG docker $USER

sudo docker run hello-world
