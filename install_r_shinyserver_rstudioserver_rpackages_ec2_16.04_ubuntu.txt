#r, shiny-server, rserver, install
#ec2 instance ubuntu lts 16.04


#start
sudo apt-get update
sudo-apt-get upgrade

#nginx
sudo apt-get -y install nginx
sudo service nginx stop
sudo service nginx start
sudo service nginx restart

sudo add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu xenial-cran35/'
sudo apt-get -y install r-base


sudo /bin/dd if=/dev/zero of=/var/swap.1 bs=1M count=1024
sudo /sbin/mkswap /var/swap.1
sudo /sbin/swapon /var/swap.1
sudo sh -c 'echo "/var/swap.1 swap swap defaults 0 0 " >> /etc/fstab'


sudo apt-get -y install libcurl4-gnutls-dev libxml2-dev libssl-dev
sudo su - -c "R -e \"install.packages('devtools', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"devtools::install_github('daattali/shinyjs')\""

#rstudio server
sudo apt-get -y install gdebi-core
wget https://download2.rstudio.org/rstudio-server-1.1.463-amd64.deb
sudo gdebi rstudio-server-1.1.463-amd64.deb

#shiny server
sudo su - -c "R -e \"install.packages('shiny', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('rmarkdown', repos='http://cran.rstudio.com/')\""
wget https://download3.rstudio.org/ubuntu-14.04/x86_64/shiny-server-1.5.9.923-amd64.deb
sudo gdebi shiny-server-1.5.9.923-amd64.deb

#add users
sudo groupadd shiny-apps
sudo usermod -aG shiny-apps ubuntu
sudo usermod -aG shiny-apps shiny
cd /srv/shiny-server
sudo chown -R ubuntu:shiny-apps .
sudo chmod g+w .
sudo chmod g+s .

#setup git
sudo apt-get -y install git
git config --global user.email "faustolopez110@gmail.com"
git config --global user.name "ulysses"

#make directory a repository
cd /srv/shiny-server
git init

#add repo
git remote add origin https://gitlab.com/ulysses/shiny-server.git
git add .
git commit -m "Initial commit"
git push -u origin master

#install gdal dependencies
sudo add-apt-repository -y ppa:ubuntugis/ubuntugis-unstable
sudo apt update
sudo apt install gdal-bin python-gdal python3-gdal libgdal1-dev
sudo apt-get install libudunits2-dev libgdal-dev libgeos-dev libproj-dev


#package installs
sudo su - -c "R -e \"install.packages('data.table', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('shinydashboard', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('ggplot2', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('scales', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('DT', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('Hmisc', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('zoo', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('plotly', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('fasttime', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('lubridate', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('dplyr', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('httr', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('jsonlite', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('RCurl', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('openxlsx', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('readxl', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('reshape', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('tidyr', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('ggmap', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('leaflet', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('raster', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('sf', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('rgdal', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('leaflet.extras', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('remotes', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('stringi', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('rgeos', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('rmapshaper', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('sp', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('echarts4r', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('shinydashboardPlus', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('shinycssloaders', repos='http://cran.rstudio.com/')\""
#sudo su - -c "R -e \"install.packages('mapview', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('shinyWidgets', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('RODBC', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('RMySQL', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('pbapply', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('filesstrings', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"devtools::install_github('ColinFay/geoloc')\""
sudo su - -c "R -e \"devtools::install_github('daattali/shinyalert')\""
sudo su - -c "R -e \"devtools::install_github('rstudio/miniUI')\""
sudo su - -c "R -e \"devtools::install_github('ebailey78/shinyBS')\""
